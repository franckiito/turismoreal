from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include('AppWeb.urls')),
    path('intranet/',include('AppIntranet.urls')),
    url(r'^khipu/', include('khipu.urls')),

] 
