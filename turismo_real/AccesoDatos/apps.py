from django.apps import AppConfig


class AccesodatosConfig(AppConfig):
    name = 'AccesoDatos'
