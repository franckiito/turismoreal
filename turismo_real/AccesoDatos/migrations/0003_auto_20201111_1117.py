# Generated by Django 2.2 on 2020-11-11 14:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('AccesoDatos', '0002_remove_usuario_fecha_nac'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usuario',
            name='fk_tipo_usuario',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='AccesoDatos.TipoUsuario'),
        ),
    ]
