from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings
#Blank false = required

# region Mantencion

class TipoUsuario(models.Model):
    nombre = models.CharField(max_length=50,blank=False)
    descripcion = models.CharField(max_length=500,blank=True)
    
    def __str__(self):
        return self.nombre


class Usuario(AbstractUser):
    fk_tipo_usuario = models.ForeignKey(TipoUsuario,null=True,blank=True,on_delete=models.CASCADE)

    rut = models.CharField(max_length=11,blank=False)
    #fecha_nac = models.DateField( blank=False)
    direccion = models.CharField(max_length=50,blank=False)
    telefono = models.CharField(max_length=12,blank=False)

    def _str_(self):
        return self.username

class ServicioExtra(models.Model):
    nombre = models.CharField(max_length=50,blank=False)
    descripcion = models.CharField(max_length=500,blank=False)
    costoDiario = models.IntegerField(blank=False)
    imagen = models.FileField(upload_to='documents/', blank=True)
    
    def __str__(self):
        return self.nombre

class Region(models.Model):
    nombre = models.CharField(max_length=50,blank=False)
    
    def __str__(self):
        return self.nombre

class Comuna(models.Model):
    fk_region = models.ForeignKey(Region,null=False,blank=False,on_delete=models.CASCADE)
    
    nombre = models.CharField(max_length=50,blank=False)
    
    def __str__(self):
        return self.nombre


class Departamento(models.Model):
    fk_comuna = models.ForeignKey(Comuna,null=True,blank=True,on_delete=models.CASCADE)
    
    direccion = models.CharField(max_length=50,blank=False)
    metros = models.IntegerField(blank=False)
    cantPersonas = models.IntegerField(blank=False)
    cantBanios = models.IntegerField(blank=False)
    cantDormitorios = models.IntegerField(blank=False)
    descripcion = models.CharField(max_length=500, blank=False)
    precioNoche = models.IntegerField(blank=False)

    estado = models.BooleanField(blank=True)
    dividendo = models.IntegerField(blank=False)
    contribuciones = models.IntegerField(blank=False)

    imagen = models.FileField(upload_to='documents/',blank=False)
    
    def __str__(self):
        return self.direccion

class Inventario(models.Model):
    fk_empleador = models.ForeignKey(Usuario,null=True,blank=True,on_delete=models.CASCADE)
    fk_departamento = models.ForeignKey(Departamento,null=False,blank=False,on_delete=models.CASCADE)

    nombre = models.CharField(max_length=50,blank=False)
    descripcion = models.CharField(max_length=500,blank=False)
    fechaRegistro = models.DateField( blank=False)
    
    def __str__(self):
        return self.nombre

class DetalleInventario(models.Model):
    fk_inventario = models.ForeignKey(Inventario,null=False,blank=False,on_delete=models.CASCADE)

    nombreObjeto = models.CharField(max_length=50,blank=False)
    cantidad = models.IntegerField(blank=False)
    descripcion = models.CharField(max_length=500,blank=True)
    costo = models.IntegerField(blank=False)
    
    def __str__(self):
        return self.nombreObjeto

class ImagenDepartamento(models.Model):
    fk_departamento = models.ForeignKey(Departamento,null=False,blank=False,on_delete=models.CASCADE)

    imagen = models.FileField(upload_to='documents/', blank=False)
    descripcion = models.CharField(max_length=500, blank=False)
    
    def __str__(self):
        return self.descripcion

class Mantencion(models.Model):
    fk_departamento = models.ForeignKey(Departamento,null=True,blank=True,on_delete=models.CASCADE)
    fk_empleado = models.ForeignKey(Usuario,null=True,blank=True,on_delete=models.CASCADE)

    nombre = models.CharField(max_length=50,blank=False)
    descripcion = models.CharField(max_length=500,blank=False)
    costo = models.IntegerField(blank=False)
    fechaRegistro = models.DateField( blank=True)
    fechaInicioProg = models.DateField( blank=False)
    fechaTerminoProg = models.DateField( blank=False)
    fechaRealInicio = models.DateField( blank=True, null=True)
    fechaRealTermino = models.DateField( blank=True, null=True)
    
    def __str__(self):
        return self.nombre

# endregion

# region Arriendo

class TipoEstado(models.Model):
    nombre = models.CharField(max_length=50,blank=False)
    descripcion = models.CharField(max_length=500,blank=True)
    
    def __str__(self):
        return self.nombre

class Arriendo(models.Model):
    fk_usuario = models.ForeignKey(Usuario,null=False,blank=False,on_delete=models.CASCADE)
    fk_tipo_estado = models.ForeignKey(TipoEstado,null=False,blank=False,on_delete=models.CASCADE)

    fechaRegistro = models.DateField( blank=False) 
    totalArriendo = models.IntegerField(blank=False)
    totalMulta = models.IntegerField(blank=True)
    totalServicio = models.IntegerField(blank=True)

    cantDias = models.IntegerField(blank=True)
    cantNinos = models.IntegerField(blank=True)
    cantAdultos = models.IntegerField(blank=True)

    cantPersonas = models.IntegerField()

    def __str__(self):
        return self.totalArriendo

class Reserva(models.Model):

    fechaInicio = models.DateField( blank=True) 
    fechaTermino = models.DateField( blank=True)
    costo = models.IntegerField()

    def __str__(self):
        return self.costo

class CheckIn(models.Model):
    fk_usuario = models.ForeignKey(Usuario,null=True,blank=True,on_delete=models.CASCADE)

    fechaRegistro = models.DateField( blank=True) 
    abonoFinal = models.IntegerField()
    observacion = models.CharField(max_length=500,blank=True)
    
    def __str__(self):
        return self.abonoFinal

class DetalleCheckIn(models.Model):
    fk_check_in = models.ForeignKey(CheckIn,null=True,blank=True,on_delete=models.CASCADE)

    nombreObjeto = models.CharField(max_length=500,blank=True)
    cantRegistrada = models.IntegerField()
    observacion = models.CharField(max_length=500,blank=True)
    
    def __str__(self):
        return self.nombreObjeto

class CheckOut(models.Model):
    fk_usuario = models.ForeignKey(Usuario,null=True,blank=True,on_delete=models.CASCADE)

    fechaRegistro = models.DateField( blank=True) 
    costoMulta = models.IntegerField()
    observacion = models.CharField(max_length=500,blank=True)

    def __str__(self):
        return self.costoMulta

class DetalleCheckOut(models.Model):
    fk_check_out = models.ForeignKey(CheckOut,null=True,blank=True,on_delete=models.CASCADE)

    nombreObjeto = models.CharField(max_length=500,blank=True)
    cantRegistrada = models.IntegerField()
    observacion = models.CharField(max_length=500,blank=True)
    
    def __str__(self):
        return self.nombreObjeto

class Multa(models.Model):
    fk_check_out = models.ForeignKey(CheckOut,null=True,blank=True,on_delete=models.CASCADE)

    nombreObjeto = models.CharField(max_length=500,blank=True)
    cantFaltante = models.IntegerField()
    observacion = models.CharField(max_length=500,blank=True)
    
    def __str__(self):
        return self.nombreObjeto

class Ocupante(models.Model):
    fk_arriendo = models.ForeignKey(Arriendo,null=True,blank=True,on_delete=models.CASCADE)
    
    rut = models.CharField(max_length=11,blank=False)
    nombres = models.CharField(max_length=50,blank=False)
    apellidos = models.CharField(max_length=50,blank=False)
    edad = models.IntegerField(blank=False)

    def __str__(self):
        return self.rut

class DetalleArriendo(models.Model):
    fk_departamento = models.ForeignKey(Departamento,null=True,blank=True,on_delete=models.CASCADE)
    fk_servicio_extra = models.ForeignKey(ServicioExtra,null=True,blank=True,on_delete=models.CASCADE)
    fk_arriendo = models.ForeignKey(Arriendo,null=True,blank=True,on_delete=models.CASCADE)
    fk_reserva = models.ForeignKey(Reserva,null=True,blank=True,on_delete=models.CASCADE)
    fk_check_in = models.ForeignKey(CheckIn,null=True,blank=True,on_delete=models.CASCADE)
    fk_check_out = models.ForeignKey(CheckOut,null=True,blank=True,on_delete=models.CASCADE)

    

    def __str__(self):
        return self.fk_arriendo

# endregion