from django.contrib import admin
from AccesoDatos.models import *

# Register your models here.

admin.site.register(Mantencion) 
admin.site.register(TipoUsuario) 
admin.site.register(Usuario) 
admin.site.register(ServicioExtra) 
admin.site.register(Region) 
admin.site.register(Comuna) 
admin.site.register(TipoEstado) 
admin.site.register(Departamento) 
admin.site.register(Inventario) 
admin.site.register(DetalleInventario) 
admin.site.register(ImagenDepartamento) 

