from django import forms
from AccesoDatos.models import *
from django.contrib.auth.forms import AuthenticationForm


# region usuario


class LoginForm(forms.Form):

    # Nombre usuario
    usr = forms.CharField(label='Nombre de usuario', max_length=20)
    usr.widget.attrs.update(
        {'class': 'form-control', 'placeholder': 'Nombre de usuario', 'required': 'required', 'minlength':'2',
            'max_length':'20'})

    # Contraseña
    pwd = forms.CharField(label='Contraseña',
                          widget=forms.PasswordInput(), max_length=20)
    pwd.widget.attrs.update(
        {'class': 'form-control', 'placeholder': 'Contraseña', 'required': 'required',})


class UsuarioForm(forms.ModelForm):

    class Meta:
        model = Usuario
        exclude = ['is_staff', 'is_superuser', 'last_login',
                   'date_joined', 'groups', 'user_permissions', 'is_active', 'fk_tipo_usuario']
        labels = {'username': 'Nombre de usuario', 'password': 'Contraseña',
                  'first_name': 'Nombre', 'last_name': 'Apellido', 'email': 'Correo',
                  'rut': 'Rut',  'direccion': 'Dirección',
                  'telefono': 'Teléfono'}

        help_texts = {'username': ' ', }

        widgets = {
            'email': forms.EmailInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Correo Electronico',
                    'required': 'required',
                }
            ),
            'first_name': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese su nombre',
                    'required': 'required',
                }
            ),
            'username': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Nombre de usuario',
                    'required': 'required',
                }
            ),
            'last_name': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese su apellido',
                    'required': 'required',
                }
            ),
            'password': forms.PasswordInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese su contraseña',
                    'required': 'required',
                }
            ),
            'rut': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese su Rut',
                    'required': 'required',
                }
            ),
            'direccion': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese su Direccion',
                    'required': 'required',
                }
            ),
            'telefono': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese su número telefónico',
                    'required': 'required',
                    'onkeypress':'return event.charCode >= 48 && event.charCode <= 57',
                }
            ),
        }

    def save(self, commit=True):  # Save the provided password in hashed format #
        user = super(UsuarioForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])

        if commit:
            user.save()
        return user


class UsuarioAddAdmForm(forms.ModelForm):
    class Meta:
        model = Usuario
        exclude = ['is_staff', 'is_superuser', 'last_login',
                   'date_joined', 'groups', 'user_permissions']
        labels = {'username': 'Nombre de usuario', 'first_name': 'Nombre', 'last_name': 'Apellidos',
                  'is_active': 'Habilitado', 'password': 'Contraseña', 'fk_tipo_usuario': 'Tipo Usuario',
                  'rut':'Rut'}
        help_texts = {'password': ' ', 'username': ' ', }

        widgets = {
            'direccion': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese direccion',
                    'required': 'required',
                }
            ),
            'password': forms.PasswordInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese contraseña',
                    'required': 'required',
                }
            ),
            'telefono': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese numero de telefono',
                    'required': 'required',
                    'onkeypress':'return event.charCode >= 48 && event.charCode <= 57',
                }
            ),
            'username': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese nombre de usuario',
                    'required': 'required',
                }
            ),
            'first_name': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese nombre',
                    'required': 'required',
                }
            ),
            'last_name': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese apellido',
                    'required': 'required',
                }
            ),
            'email': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese correo',
                    'required': 'required',
                }
            ),
            'rut': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese rut',
                    'required': 'required',
                }
            ),
            'fk_tipo_usuario': forms.Select(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese Tipo usuario',
                    'required': 'required',
                }
            ),
            
        }
    def save(self, commit=True):  # Save the provided password in hashed format #
        user = super(UsuarioForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])

        if commit:
            user.save()
        return user

class UsuarioAdmForm(forms.ModelForm):
    class Meta:
        model = Usuario
        exclude = ['is_staff', 'is_superuser', 'last_login',
                   'date_joined', 'groups', 'user_permissions', 'password']
        labels = {'username': 'Nombre de usuario', 'first_name': 'Nombre',
                  'last_name': 'Apellidos',  'is_active': 'Habilitado'}
        help_texts = {'password': ' ', 'username': ' ', }

        widgets = {
            'direccion': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese direccion',
                    'required': 'required',
                }
            ),
            'telefono': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese numero de telefono',
                    'required': 'required',
                }
            ),
            'username': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese nombre de usuario',
                    'required': 'required',
                }
            ),
            'first_name': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese nombre',
                    'required': 'required',
                }
            ),
            'last_name': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese apellido',
                    'required': 'required',
                }
            ),
            'email': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese correo',
                    'required': 'required',
                }
            ),
            'rut': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese rut',
                    'required': 'required',
                }
            ),
            'fk_tipo_usuario': forms.Select(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese Tipo usuario',
                    'required': 'required',
                }
            ),
            
        }


class UsuarioFormSu(forms.ModelForm):
    class Meta:
        model = Usuario
        exclude = []
        widgets = {
            'password': forms.PasswordInput(),
        }

# endregion

# region mantenedores


class ServicioExtraForm(forms.ModelForm):
    class Meta:
        model = ServicioExtra
        labels = {'nombre': 'Nombre', 'descripcion': 'Descripcion',
                  'costoDiario': 'Costo Diario'}
        exclude = []
        widgets = {
            'nombre': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese nombre del servicio',
                    'id': 'nombre',
                    'required': 'required',
                }
            ),
            'descripcion': forms.Textarea(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese una descripción',
                    'id': 'descripcion',
                    'required': 'required',
                }
            ),
            'costoDiario': forms.NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese valor diario',
                    'id': 'costoDiario',
                    'required': 'required',
                }
            ),
            'imagen': forms.FileInput(
                attrs={
                    'accept':".jpg,.jpeg,.png",
                    'required': 'required',
                }
            ),
        }


class TipoEstadoForm(forms.ModelForm):
    class Meta:
        model = TipoEstado
        fields = ('nombre', 'descripcion')
        widgets = {
            'nombre': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese el estado',
                    'required': 'required',
                }
            ),
            'descripcion': forms.Textarea(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese una descripción',
                }
            ),
        }


class InventarioForm(forms.ModelForm):
    class Meta:
        model = Inventario
        exclude = ['fk_departamento', 'fk_empleador','fechaRegistro']

        widgets = {
            'nombre': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese nombre de inventario',
                    'required': 'required',
                }
            ),
            'descripcion': forms.Textarea(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese una descripción',
                }
            ),
        }


class DepartamentoForm(forms.ModelForm):
    class Meta:
        model = Departamento
        exclude = ['']
        labels = {'fk_tipo_estado': 'Estado Departamento', 'fk_comuna': 'Comuna', 'cantPersonas': 'Cantidad de Personas',
                  'cantBanios': 'Cantidad de Baños', 'cantDormitorios': 'Cantidad de Dormitorios', 'precioNoche': 'Precio por Noche',
                  'estado': 'Habilitado', 'dividendo': 'Costo dividendo', 'contribuciones': 'Costo contribuciones'
                  }

        widgets = {
            'direccion': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese la dirección',
                    'required': 'required',
                }
            ),
            'imagen': forms.FileInput(
                attrs={
                    'accept':".jpg,.jpeg,.png",
                    'required': 'required',
                }
            ),
            'metros': forms.NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Metros cuadrados',
                    'required': 'required',
                }
            ),
            'cantPersonas': forms.NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese capacidad de personas',
                    'required': 'required',
                }
            ),
            'cantBanios': forms.NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese cantidad de baños',
                    'required': 'required',
                }
            ),
            'cantDormitorios': forms.NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese cantidad de dormitorios',
                    'required': 'required',
                }
            ),
            'descripcion': forms.Textarea(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese una descripción',
                }
            ),
            'precioNoche': forms.NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese el valor por noche',
                    'required': 'required',
                }
            ),
            'estado': forms.CheckboxInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Departamento habilidado',
                }
            ),
            'dividendo': forms.NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese el valor dividendo',
                    'required': 'required',
                }
            ),
            'contribuciones': forms.NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese el valor contribuciones',
                    'required': 'required',
                }
            )
        }


class DetalleInventarioForm(forms.ModelForm):
    class Meta:
        model = DetalleInventario
        exclude = ['fk_inventario']
        labels = {'nombreObjeto': 'Nombre Objeto', 'costo': 'Costo Unitario'}

        widgets = {
            'nombreObjeto': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese nombre de objeto',
                    'required': 'required',
                }
            ),
            'cantidad': forms.NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese cantidad',
                    'required': 'required',
                }
            ),
            'costo': forms.NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese su valor',
                    'required': 'required',
                }
            ),
            'descripcion': forms.Textarea(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese una descripción',
                    'required': 'required',
                }
            ),
        }


class ImagenDepartamentoForm(forms.ModelForm):
    class Meta:
        model = ImagenDepartamento
        exclude = ['fk_departamento']

        widgets = {
            'descripcion': forms.Textarea(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese una descripción',
                    'required': 'required',
                }
            ),
            'imagen': forms.FileInput(
                attrs={
                    'accept':".jpg,.jpeg,.png",
                    'required': 'required',
                }
            ),
        }


class MantencionForm(forms.ModelForm):
    class Meta:
        model = Mantencion
        exclude = ['fk_departamento','fk_empleado', 'fechaRegistro']
        labels = { 'nombre': 'Nombre', 'costo': 'Costo', 'fechaInicioProg': 'Fecha Inicio', 'fechaTerminoProg': 'Fecha Termino', 
            'fechaRealInicio': 'Fecha Real Inicio', 'fechaRealTermino': 'Fecha Real Termino'}

        widgets = {
            'nombre': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese nombre mantencion',
                    'required': 'required',
                }
            ),
            'descripcion': forms.Textarea(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese una descripción',
                    'required': 'required',
                }
            ),
            'costo': forms.NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese costo',
                    'required': 'required',
                }
            ),
            'fechaInicioProg': forms.DateInput(
                attrs={
                    'class': 'form-control datepicker',
                    'placeholder': 'Ingrese fecha de inicio',
                    'required': 'required',
                }
            ),
            'fechaTerminoProg': forms.DateInput(
                attrs={
                    'class': 'form-control datepicker',
                    'placeholder': 'Ingrese fecha de término',
                    'required': 'required',
                }
            ),
            'fechaRealInicio': forms.DateInput(
                attrs={
                    'class': 'form-control datepicker',
                    'placeholder': 'Ingrese fecha real inicio',
                }
            ),
            'fechaRealTermino': forms.DateInput(
                attrs={
                    'class': 'form-control datepicker',
                    'placeholder': 'Ingrese fecha real Termino',
                }
            ),
        }

# endregion

# region Reserva


class ReservaForm(forms.Form):

    # Cantidad Niños
    cantNinos = forms.IntegerField(label='Cantidad Niños', required=True)
    cantNinos.widget.attrs.update(
        {'class': 'form-control', 'placeholder': 'Cantidad Niños', 'onchange': 'fn_ninos(this.value)'})

    # Cantidad Adultos
    cantAdultos = forms.IntegerField(label='Cantidad Adultos', required=True)
    cantAdultos.widget.attrs.update(
        {'class': 'form-control ', 'placeholder': 'Cantidad Adultos', 'onchange': 'fn_adultos(this.value)'})

    # Fecha Inicio
    fechaInicio = forms.DateField(
        label='Fecha Inicio', widget=forms.DateInput(), required=True)
    fechaInicio.widget.attrs.update({'class': 'form-control datepicker',
                                     'placeholder': 'Fecha Inicio', 'onchange': 'fn_inicio(this.value)'})

    # Fecha Fin
    fechaFin = forms.DateField(
        label='Fecha Termino', widget=forms.DateInput(), required=True)
    fechaFin.widget.attrs.update({'class': 'form-control datepicker',
                                  'placeholder': 'Fecha Termino', 'onchange': 'fn_termino(this.value)'})

    # Hiddens
    cantPersonas = forms.CharField(widget=forms.HiddenInput())
    cantDias = forms.CharField(widget=forms.HiddenInput())
    totalArriendo = forms.CharField(widget=forms.HiddenInput())
    abono = forms.CharField(widget=forms.HiddenInput())

class DisponibilidadForm(forms.Form):

    # Fecha Inicio
    fechaInicio = forms.DateField(
        label='Fecha Inicio', widget=forms.DateInput(), required=True)
    fechaInicio.widget.attrs.update({'class': 'form-control daterange-picker',
                                     'placeholder': 'Fecha Inicio', 'onchange': 'fn_inicio(this.value)'})

    # Fecha Fin
    fechaFin = forms.DateField(
        label='Fecha Termino', widget=forms.DateInput(), required=True)
    fechaFin.widget.attrs.update({'class': 'form-control daterange-picker',
                                  'placeholder': 'Fecha Termino', 'onchange': 'fn_termino(this.value)'})


class OcupanteForm(forms.ModelForm):
    class Meta:
        model = Ocupante
        exclude = []
        widgets = {
            'rut': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese rut',
                    'id': 'rut',
                    'required': 'required',
                }
            ),
            'nombres': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese nombres',
                    'id': 'nombres',
                    'required': 'required',
                }
            ),
            'apellidos': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese apellidos',
                    'id': 'apellidos',
                    'required': 'required',
                }
            ),
            'edad': forms.NumberInput(
                attrs={
                    'class': 'form-control',
                    'type': 'number',
                    'placeholder': 'Ingrese edad',
                    'id': 'edad',
                    'min': '0',
                    'max': '99',
                    'required': 'required',
                }
            ),
        }


# endregion


