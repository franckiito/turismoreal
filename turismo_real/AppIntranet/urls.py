from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

from django.conf.urls import url
from AppIntranet.views import CreateDepartamentoView, EditDepaView, ListDepartamentoView
from AppIntranet.views import CreateServicioExtraView, EditServicioExtraView, ListServicioExtraView
from AppIntranet.views import listado_tours,servicios,otros_servicios,CreateServicioExtraView,EditServicioExtraView
from AppIntranet.views import CreateInventarioView, EditInventarioView, ListInventarioView
from AppIntranet.views import CreateUsuarioView, EditUsuarioView, ListUsuarioView
from AppIntranet.views import CreateDetalleInventarioView, EditDetalleInventarioView, ListDetalleInventarioView
from AppIntranet.views import CreateTipoEstado, TipoEstadoEditView, ListTipoEstadoView
from AppIntranet.views import CreateImagenDepartamentoView, EditImagenDepartamentoView, ListImagenDepartamentoView
from AppIntranet.views import CreateMantencionView, EditMantencionView, ListMantencionView
from AppIntranet.views import LoginAdmView, LogoutView

from AppIntranet.views import ReporteView, AgendaView, MantenedoresView, CheckinView
from AppIntranet.views import asignaciones_transporte,asignacion_conductor,asignacion_vehiculo
from AppIntranet.views import CheckOutView

urlpatterns = [
    
    path('index',views.index, name = "intranet"),

    url(r'^agenda$', AgendaView.as_view(), name='agenda'),
    url(r'^checkin$', CheckinView.as_view(), name='checkin'),
    url(r'^checkout$', CheckOutView.as_view(), name='check_out'),
    url(r'^mantenedores$', MantenedoresView.as_view(), name='mantenedores'),


    url(r'^reporte$', ReporteView.as_view(), name='reporte'),
    url(r'^estado/all$', ListTipoEstadoView.as_view(), name='list_estados'),
    url(r'^estado/new_estado$', CreateTipoEstado.as_view(), name='create_estado'),

    
    url(r'^departamento/add$', CreateDepartamentoView.as_view(), name='create_depa'),
    url(r'^departamento/edit/(?P<pk>[0-9]+)$', EditDepaView.as_view(), name='edit_depa'),
    url(r'^departamento/all$', ListDepartamentoView.as_view(), name='list_depa'),

    url(r'^otros_servicios/add$', CreateServicioExtraView.as_view(), name='create_serv'),
    url(r'^otros_servicios/edit/(?P<pk>[0-9]+)$', EditServicioExtraView.as_view(), name='edit_serv'),
    url(r'^otros_servicios/all$', ListServicioExtraView.as_view(), name='list_serv'),

    
    path('servicios/', views.servicios, name='list_serv_all'),
    path('otros_servicios/', views.otros_servicios, name='list_otros_serv'),

    path('aniadir_tour/', views.añadir_tour, name='add_tour'),
    path('editar_tour/', views.editar_tour, name='edit_tour'),
    path('tours_disponibles/', views.listado_tours, name='list_tours'),
    path('mantenedor_transporte/', views.transporte, name='list_transporte'),
    path('asignaciones_transporte/', views.asignaciones_transporte, name='asig_transporte'),
    path('asignacion_conductor/', views.asignacion_conductor, name='asig_conductor'),
    path('asignacion_vehiculo/', views.asignacion_vehiculo, name='asig_vehiculo'),

    url(r'^servicio/add$', CreateServicioExtraView.as_view(), name='create_serv'),
    url(r'^servicio/edit/(?P<pk>[0-9]+)$', EditServicioExtraView.as_view(), name='edit_serv'),
    url(r'^servicio/all$', ListServicioExtraView.as_view(), name='list_serv'),

    url(r'^inventario/add$', CreateInventarioView.as_view(), name='create_inv'),
    url(r'^inventario/edit/(?P<pk>[0-9]+)$', EditInventarioView.as_view(), name='edit_inv'),
    url(r'^inventario/all/(?P<pk>[0-9]+)$', ListInventarioView.as_view(), name='list_inv'),

    url(r'^usuario/add$', CreateUsuarioView.as_view(), name='create_usr'),
    url(r'^usuario/edit/(?P<pk>[0-9]+)$', EditUsuarioView.as_view(), name='edit_usr'),
    url(r'^usuario/all$', ListUsuarioView.as_view(), name='list_usr'),

    
    url(r'^tipoestado/add$', CreateTipoEstado.as_view(), name='create_tipoestado'),
    url(r'^tipoestado/edit/(?P<pk>[0-9]+)$', TipoEstadoEditView.as_view(), name='edit_tipoestado'),
    url(r'^tipoestado/all$', ListTipoEstadoView.as_view(), name='list_tipoestado'),

    url(r'^detalle/add$', CreateDetalleInventarioView.as_view(), name='create_deta'),
    url(r'^detalle/edit/(?P<pk>[0-9]+)$', EditDetalleInventarioView.as_view(), name='edit_deta'),
    url(r'^detalle/all/(?P<pk>[0-9]+)$', ListDetalleInventarioView.as_view(), name='list_deta'),

    url(r'^imagen/add$', CreateImagenDepartamentoView.as_view(), name='create_img'),
    url(r'^imagen/edit/(?P<pk>[0-9]+)$', EditImagenDepartamentoView.as_view(), name='edit_img'),
    url(r'^imagen/all/(?P<pk>[0-9]+)$', ListImagenDepartamentoView.as_view(), name='list_img'),

    url(r'^mantencion/add$', CreateMantencionView.as_view(), name='create_mant'),
    url(r'^mantencion/edit/(?P<pk>[0-9]+)$', EditMantencionView.as_view(), name='edit_mant'),
    url(r'^mantencion/all/(?P<pk>[0-9]+)$', ListMantencionView.as_view(), name='list_mant'),

    path('', LoginAdmView.as_view(), name='intranet_login'), 
    url(r'^logout$', LogoutView.as_view(), name='intranet_logout'),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

