from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic import View, ListView
from AccesoDatos.models import Departamento, Inventario, DetalleInventario, Region, ServicioExtra, Usuario
from AccesoDatos.forms import DepartamentoForm, InventarioForm, DetalleInventarioForm, ServicioExtraForm, UsuarioAdmForm, UsuarioAddAdmForm, LoginForm
from AccesoDatos.models import Region, Comuna, TipoEstado, Inventario, Departamento, ImagenDepartamento, Mantencion, DetalleInventario
from AccesoDatos.forms import TipoEstadoForm, ImagenDepartamentoForm, MantencionForm
from django.contrib.auth import logout as django_logout, authenticate, login as django_login

from AccesoDatos.models import Arriendo, DetalleArriendo, TipoEstado

import datetime
from datetime import date
from django.core.serializers import serialize
from django.core.serializers.json import DjangoJSONEncoder

class LazyEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if isinstance(obj, YourCustomType):
            return str(obj)
        return super().default(obj)

# Create your views here.

# region Templetes

class ReporteView(View):
    def get(self, request):
        return render(request, "AppIntranet/reporte.html")

class MantenedoresView(View):
    def get(self, request):
        return render(request, "AppIntranet/mantenedores.html")

class CheckinView(View):
    def get(self, request):
        return render(request, "AppIntranet/checkin.html")
class CheckOutView(View):
    def get(self, request):
        return render(request, "AppIntranet/check_out/check_out.html")

# endregion

#region FUNCIONARIO

class AgendaView(View):
    def get(self, request):
        arriendos = Arriendo.objects.only('id').all()

        #obtener arriendos que estan en reserva(aun no hacen check-in)
        reserva = TipoEstado.objects.filter(nombre="En Reserva")[0]
        arriendos_reserva = Arriendo.objects.filter(fk_tipo_estado=reserva).only('id')
        check_in = DetalleArriendo.objects.filter(fk_arriendo__in=arriendos_reserva)

        #obtener arriendos que estan siendo ocupados(ya paso check-in)
        arriendo = TipoEstado.objects.filter(nombre="En arriendo")[0]
        arriendos_ocupando = Arriendo.objects.filter(fk_tipo_estado=arriendo).only('id')
        check_out = DetalleArriendo.objects.filter(fk_arriendo__in=arriendos_ocupando)
        
        today = date.today()
        today = today.strftime("%Y-%m-%d")
        
        context = {
            "check_in" : check_in,
            "check_out" : check_out,
            "today" : today,
        }

        return render(request, "AppIntranet/agenda.html", context)

#endregion

#region INICIO SESION

#Login y logout
class LoginAdmView(View):
    def get(self,request):
        error_messages = []
        form = LoginForm()
        context = {
            'errors': error_messages,
            'form': form
        }
        return render(request,'AppIntranet/login.html', context)

    def post(self, request):
        error_messages = []
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('usr')
            password = form.cleaned_data.get('pwd')
            user = authenticate(username=username, password=password)
            if user is None:
                
                data = {
                    'mensaje': 'Nombre de usuario o contraseña incorrectos.',
                    'type': 'error',
                    'tittle': 'Inicio de sesion'
                }
                return JsonResponse(data)
                
                """
                print(user)
                return render(request, "AppIntranet/login.html", {'mensaje': 'Nombre de usuario o contraseña incorrectos..','form':form})
                """
            else:
                if user.is_active:
                    django_login(request, user)

                    data = {
                        'mensaje': 'Bienvenido a Turismo Real.',
                        'type': 'success',
                        'tittle': 'Inicio de sesion'
                    }
                    return JsonResponse(data)

                    #url = request.GET.get('next', 'mi_perfil')
                    #return redirect(url)
                    #return render(request, "AppIntranet/index.html")
                else:
                    data = {
                        'mensaje': 'Debe activar su cuenta.',
                        'type': 'warning',
                        'tittle': 'Inicio de sesion'
                    }
                    return JsonResponse(data)

                    #return render(request, "AppIntranet/login.html", {'mensaje': 'usuario inactivo.', 'form':form})
                

class LogoutView(View):
    def get(self,request):
        if request.user.is_authenticated:
            django_logout(request)
        return redirect('/intranet')


def index(request):
    return render(request, "AppIntranet/index.html")


class intranetView(View):
    @method_decorator(login_required())
    def get(self, request):
        return render(request, "AppIntranet/index.html", context)

def mantenedor_depa(request):
    return render(request, 'AppIntranet/man_depto.html')

# endregion Inicio Sesion

# region USUARIO

# Query que retorna los Usuario
class UsuarioQueryset(object):
    @classmethod
    def get_usuarios_queryset(cls):
        usuarios = Usuario.objects.all()
        return usuarios

# vista de los Usuario
class ListUsuarioView(View):
    def get(self, request):
        usuarios_list = Usuario.objects.all()
        context = {
            "usuarios_list": usuarios_list
        }
        return render(request, "AppIntranet/usuario/mantenedor_usuario.html", context)

# Crea Usuario
class CreateUsuarioView(View):

    #@method_decorator(login_required())
    def get(self, request):
        form = UsuarioAddAdmForm()
        msg = ['default', ]

        context = {
            'form': form,
            'msg': msg
        }
        return render(request, 'AppIntranet/usuario/add_usuario.html', context)

    #@method_decorator(login_required())
    def post(self, request):
        usr = Usuario()
        form = UsuarioAddAdmForm(request.POST, instance=usr)
        
        if form.is_valid():
            form.save()
            data = {
                'mensaje': 'El Usuario fue ingresado correctamente',
                'type': 'success',
                'tittle': 'Usuario'
            }
            return JsonResponse(data)

        else:
            data = {
                'mensaje': 'No se pudo agregar el Usuario',
                'type': 'error',
                'tittle': 'Usuario'
            }
            return JsonResponse(data)

# Editar Usuario
class EditUsuarioView(View):
    #@method_decorator(login_required())
    def get(self, request, pk):
        possible_usuarios = Usuario.objects.filter(pk=pk)
        usr = possible_usuarios[0] if len(possible_usuarios) == 1 else None
        msg = ['default', ]
        form = UsuarioAdmForm()
        if usr:
            form = UsuarioAdmForm(instance=usr)

        context = {
            'form': form,
            'msg': msg,
            'pk':pk
        }
        return render(request, 'AppIntranet/usuario/edit_usuario.html', context)

    #@method_decorator(login_required())
    def post(self, request, pk):
        possible_usuarios = Usuario.objects.filter(pk=pk)
        usr = possible_usuarios[0] if len(possible_usuarios) == 1 else None        
        
        form = UsuarioAdmForm(request.POST, instance=usr)
        if form.is_valid():
            form.save()
            data = {
                'mensaje': 'El Usuario fue editado correctamente',
                'type': 'success',
                'tittle': 'Usuario'
            }
            return JsonResponse(data)
        else:
            data = {
                'mensaje': 'No se pudo editar el Usuario',
                'type': 'error',
                'tittle': 'Usuario'
            }
            return JsonResponse(data)
        

# endregion Usuario

# region INVENTARIO

# Query que retorna los Inventario
class InventarioQueryset(object):
    @classmethod
    def get_inventarios_queryset(cls):
        inventarios = Inventario.objects.all()
        return inventarios

# vista de los Inventario
class ListInventarioView(View):
    def get(self, request,pk):
        inventarios_list = Inventario.objects.filter(fk_departamento=pk)
        depa = Departamento.objects.filter(pk=pk)[0]
        context = {
            "inventarios_list": inventarios_list,
            'departamento': depa,
        }
        request.session['pk_departamento'] = pk
        return render(request, "AppIntranet/inventario/mantenedor_inventario.html", context)

# Crea Inventario
class CreateInventarioView(View):

    #@method_decorator(login_required())
    def get(self, request):
        form = InventarioForm()
        pk = request.session.get('pk_departamento')
        depa = Departamento.objects.filter(pk=pk)[0]
        context = {
            'form': form,
            'pk': pk,
            'departamento': depa,
        }
        return render(request, 'AppIntranet/inventario/add_inventario.html', context)

    #@method_decorator(login_required())
    def post(self, request):

        pk = request.session.get('pk_departamento')
        if pk:
            possible_depas = Departamento.objects.filter(pk=pk)
            depa = possible_depas[0] if len(possible_depas) == 1 else None
            users = Usuario.objects.filter(pk=request.user.pk)
            user = users[0] if len(users) == 1 else None
            if (depa is not None) and (user is not None):
                            
                inv = Inventario()
                inv.fk_departamento = depa
                inv.fk_empleador = user
                inv.fechaRegistro = date.today()
                form = InventarioForm(request.POST,  instance=inv)
                
                if form.is_valid():
                    form.save()
                    data = {
                        'mensaje': 'El inventario fue ingresado correctamente',
                        'type': 'success',
                        'tittle': 'Inventario'
                    }
                    return JsonResponse(data)

                else:
                    data = {
                        'mensaje': 'No se pudo agregar el inventario',
                        'type': 'error',
                        'tittle': 'Inventario'
                    }
                    return JsonResponse(data)
            else:
                data = {
                    'mensaje': 'Error al asignar pk depa',
                    'type': 'error',
                    'tittle': 'Inventario'
                }
                return JsonResponse(data)
        else:
            context = {
                'msg': 'No hay pk session'
            }
            return render(request, 'AppIntranet/inventario/add_inventario.html', context)

# Editar Inventario
class EditInventarioView(View):
    #@method_decorator(login_required())
    def get(self, request, pk):
        inventario = Inventario.objects.filter(pk=pk)[0]
        possible_invents = Inventario.objects.filter(pk=pk)
        inv = possible_invents[0] if len(possible_invents) == 1 else None
        form = InventarioForm()
        if inv:
            form = InventarioForm(instance=inv)
        pk_depa = request.session.get('pk_departamento')
        depa = Departamento.objects.filter(pk=pk_depa)[0]
        context = {
            'form': form,
            'pk': pk_depa,
            'id': inv.pk,
            'departamento': depa,
            'inv': inventario,
        }
        return render(request, 'AppIntranet/inventario/edit_inventario.html', context)

    #@method_decorator(login_required())
    def post(self, request, pk):
        possible_invents = Inventario.objects.filter(pk=pk)
        inv = possible_invents[0] if len(possible_invents) == 1 else None
        msg = ['default', ]
        
        form = InventarioForm(request.POST,request.FILES, instance=inv)
        if form.is_valid():
            form.save()
            data = {
                'mensaje': 'El inventario fue editado correctamente',
                'type': 'success',
                'tittle': 'Inventario'
            }
            return JsonResponse(data)
        else:
            data = {
                'mensaje': 'No se pudo editar el inventario',
                'type': 'error',
                'tittle': 'Inventario'
            }
            return JsonResponse(data)
        

# endregion

# region DETALLE INVENTARIO

# Query que retorna los DetalleInventario
class DetalleInventarioQueryset(object):
    @classmethod
    def get_detalles_inventario_queryset(cls):
        detalles = DetalleInventario.objects.all()
        return detalles

# vista de los DetalleInventario
class ListDetalleInventarioView(View):
    def get(self, request,pk):
        detalles_list = DetalleInventario.objects.filter(fk_inventario=pk)
        pk_depa = request.session.get('pk_departamento')
        
        depa = Departamento.objects.filter(pk=pk_depa)[0]
        inv = Inventario.objects.filter(pk=pk)[0]
        context = {
            "detalles_list": detalles_list,
            'departamento': depa,
            'inv': inv,
        }
        request.session['pk_inventario'] = pk
        return render(request, "AppIntranet/detalle/mantenedor_detalle_inv.html", context)

# Crea DetalleInventario
class CreateDetalleInventarioView(View):

    #@method_decorator(login_required())
    def get(self, request):
        form = DetalleInventarioForm()
        pk_inv = request.session.get('pk_inventario')
        inv = Inventario.objects.filter(pk=pk_inv)[0]
        pk_depa = request.session.get('pk_departamento')
        depa = Departamento.objects.filter(pk=pk_depa)[0]
        context = {
            'form': form,
            'inv': inv,
            'departamento': depa,
        }
        return render(request, 'AppIntranet/detalle/add_detalle_inv.html', context)

    #@method_decorator(login_required())
    def post(self, request):

        pk = request.session.get('pk_inventario')
        if pk:
            possible_invents = Inventario.objects.filter(pk=pk)
            inv = possible_invents[0] if len(possible_invents) == 1 else None
            if inv is not None:
                            
                deta = DetalleInventario()
                deta.fk_inventario = inv
                form = DetalleInventarioForm(request.POST,  instance=deta)
                
                if form.is_valid():
                    form.save()
                    data = {
                        'mensaje': 'El objeto fue ingresado correctamente',
                        'type': 'success',
                        'tittle': 'Detalle Inventario'
                    }
                    return JsonResponse(data)

                else:
                    data = {
                        'mensaje': 'No se pudo agregar el objeto',
                        'type': 'error',
                        'tittle': 'Detalle Inventario'
                    }
                    return JsonResponse(data)
            else:
                data = {
                    'mensaje': 'No se encontro el Inventario',
                    'type': 'error',
                    'tittle': 'Detalle Inventario'
                }
                return JsonResponse(data)
        else:
            context = {
                'msg': 'No hay pk session'
            }
            return render(request, 'AppIntranet/detalle/add_detalle_inv.html', context)

# Editar DetalleInventario
class EditDetalleInventarioView(View):
    #@method_decorator(login_required())
    def get(self, request, pk):
        detalle = DetalleInventario.objects.filter(pk=pk)[0]
        possible_detas = DetalleInventario.objects.filter(pk=pk)
        deta = possible_detas[0] if len(possible_detas) == 1 else None
        form = DetalleInventarioForm()
        if deta:
            form = DetalleInventarioForm(instance=deta)
        pk_inv = request.session.get('pk_inventario')
        inv = Inventario.objects.filter(pk=pk_inv)[0]
        pk_depa = request.session.get('pk_departamento')
        depa = Departamento.objects.filter(pk=pk_depa)[0]

        context = {
            'form': form,
            'inv': inv,
            'departamento': depa,
            'id': deta.pk,
            'detalle': detalle,
        }
        return render(request, 'AppIntranet/detalle/edit_detalle_inv.html', context)

    #@method_decorator(login_required())
    def post(self, request, pk):
        possible_detas = DetalleInventario.objects.filter(pk=pk)
        deta = possible_detas[0] if len(possible_detas) == 1 else None
        
        form = DetalleInventarioForm(request.POST,request.FILES, instance=deta)
        if form.is_valid():
            form.save()
            data = {
                'mensaje': 'El objeto fue editado correctamente',
                'type': 'success',
                'tittle': 'Detalle Inventario'
            }
            return JsonResponse(data)
        else:
            data = {
                'mensaje': 'No se pudo editar el objeto',
                'type': 'error',
                'tittle': 'Detalle Inventario'
            }
            return JsonResponse(data)
        

# endregion

# region Departamento

# Query que retorna los Departamento
class DepartamentoQueryset(object):
    @classmethod
    def get_departamentos_queryset(cls):
        departamentos = Departamento.objects.all()
        return departamentos

# vista de los Departamentos


class ListDepartamentoView(View):
    def get(self, request):
        departamentos_list = Departamento.objects.all()
    
        context = {
            "departamentos_list": departamentos_list,
        }
        return render(request, "AppIntranet/departamento/man_depto.html", context)

# Crea departamento
class CreateDepartamentoView(View):

    #@method_decorator(login_required())
    def get(self, request):
        form = DepartamentoForm()

        #comunas = Comuna.objects.all()
        #regiones = Region.objects.all()

        context = {
            'form': form,
            #'regiones': regiones,
            #'comunas': comunas,
        }
        return render(request, 'AppIntranet/departamento/add_departamento.html', context)

    #@method_decorator(login_required())
    def post(self, request):
        depa = Departamento()
        form = DepartamentoForm(request.POST, request.FILES, instance=depa)
        
        if form.is_valid():
            form.save()
            data = {
                'mensaje': 'El departamento fue ingresado correctamente',
                'type': 'success',
                'tittle': 'Departamento'
            }
            return JsonResponse(data)

        else:
            data = {
                'mensaje': 'No se pudo agregar el departamento',
                'type': 'error',
                'tittle': 'Departamento'
            }
            return JsonResponse(data)

# Editar departament
class EditDepaView(View):
    #@method_decorator(login_required())
    def get(self, request, pk):
        possible_depas = Departamento.objects.filter(pk=pk)
        depa = possible_depas[0] if len(possible_depas) == 1 else None
        msg = ['default', ]
        form = DepartamentoForm()
        if depa:
            form = DepartamentoForm(instance=depa)

        context = {
            'form': form,
            'msg': msg,
            'pk': depa.pk,
            'depa': depa,
        }
        return render(request, 'AppIntranet/departamento/edit_departamento.html', context)

    #@method_decorator(login_required())
    def post(self, request, pk):
        possible_depas = Departamento.objects.filter(pk=pk)
        depa = possible_depas[0] if len(possible_depas) == 1 else None
        msg = ['default', ]
        
        form = DepartamentoForm(request.POST,request.FILES, instance=depa)
        if form.is_valid():
            form.save()
            data = {
                'mensaje': 'El departamento fue editado correctamente',
                'type': 'success',
                'tittle': 'Departamento'
            }
            return JsonResponse(data)
        else:
            data = {
                'mensaje': 'No se pudo editar el departamento',
                'type': 'error',
                'tittle': 'Departamento'
            }
            return JsonResponse(data)
        

# endregion

# region SERVICIO EXTRA

# Query que retorna los ServicioExtra
# vista de Tours

def listado_tours(request):

    return render(request, "AppIntranet/servicios/tours/mantenedor_tours.html")

# vista Añadir tour

def añadir_tour(request, context):

    return render(request, "AppIntranet/servicios/tours/mantenedor_tours.html", context)

# vista editar tour

def editar_tour(request, context):

    return render(request, "AppIntranet/servicios/tours/mantenedor_tours.html", context)

def transporte(request):
    return render(request, "AppIntranet/servicios/transporte/mantenedor_transporte.html")

def asignaciones_transporte(request):
    return render(request, "AppIntranet/servicios/transporte/asignaciones_transporte.html")

# vista de los Servicios

def servicios(request):
    return render(request, "AppIntranet/servicios/all_services.html")


# vista de los otro Servicios

def otros_servicios(request):
    return render(request, "AppIntranet/servicios/otros_servicios/mantenedor_otros_servicios.html")

# vista asignacion de conductor

def asignacion_conductor(request):
    return render(request, "AppIntranet/servicios/transporte/asig_conductor.html")

# vista asignacion de vehículo

def asignacion_vehiculo(request):
    return render(request, "AppIntranet/servicios/transporte/asig_vehiculo.html")    

class ServicioExtraQueryset(object):
    @classmethod
    def get_servicios_queryset(cls):
        servicios = ServicioExtra.objects.all()
        return servicios

# vista de los ServicioExtra


class ListServicioExtraView(View):
    def get(self, request):
        servicios_list = ServicioExtra.objects.all()
        context = {
            "servicios_list": servicios_list
        }
        return render(request, "AppIntranet/servicios/otros_servicios/mantenedor_otros_servicios.html", context)

# Crea ServicioExtra


class CreateServicioExtraView(View):

    #@method_decorator(login_required())
    def get(self, request):
        form = ServicioExtraForm()
        msg = ['default', ]

        context = {
            'form': form,
            'msg': msg
        }
        return render(request, 'AppIntranet/servicios/otros_servicios/add_servicio_extra.html', context)

    #@method_decorator(login_required())
    def post(self, request):
        serv = ServicioExtra()
        form = ServicioExtraForm(request.POST, request.FILES, instance=serv)
        msg = []
        if form.is_valid():
            nombre = request.POST['nombre']
            possible_servis = ServicioExtra.objects.filter(nombre=nombre)
            serv = possible_servis[0] if len(possible_servis) == 1 else None
            if serv is None:
                form.save()
                data = {
                    'mensaje': 'El Servicio Extra fue creado correctamente',
                    'type': 'success',
                    'tittle': 'Registro de Servicio Extra'
                }
                return JsonResponse(data)
            else:
                data = {
                    'mensaje': 'El Servicio Extra ya existe',
                    'type': 'error',
                    'tittle': 'Registro de Servicio Extra'
                }
                return JsonResponse(data)

        else:
            data = {
                'mensaje': 'Problemas con el form Servicio',
                'type': 'error',
                'tittle': 'Registro de Servicio Extra'
            }
            return JsonResponse(data)

# Editar ServicioExtra
class EditServicioExtraView(View):
    #@method_decorator(login_required())
    def get(self, request, pk):
        possible_servis = ServicioExtra.objects.filter(pk=pk)
        serv = possible_servis[0] if len(possible_servis) == 1 else None
        msg = ['default', ]
        form = ServicioExtraForm()
        if serv:
            form = ServicioExtraForm(instance=serv)

        context = {
            'form': form,
            'msg': msg,
            'pk':pk
        }
        return render(request, 'AppIntranet/servicios/otros_servicios/edit_servicio_extra.html', context)

    #@method_decorator(login_required())
    def post(self, request, pk):
        try:
            possible_servis = ServicioExtra.objects.filter(pk=pk)
            serv = possible_servis[0] if len(possible_servis) == 1 else None
            msg = ['default', ]
            
            form = ServicioExtraForm(request.POST, request.FILES, instance=serv)
            if form.is_valid():
                
                form.save()
                data = {
                    'mensaje': 'El Servicio Extra fue editado correctamente',
                    'type': 'success',
                    'tittle': ' Servicio Extra'
                }
                return JsonResponse(data)
            else:
                data = {
                    'mensaje': 'El Servicio Extra no se pudo editar',
                    'type': 'error',
                    'tittle': 'Servicio Extra'
                }
                return JsonResponse(data)
        except :
            data = {
                'mensaje': 'Error al editar servicio extra',
                'type': 'error',
                'tittle': ' Servicio Extra'
            }
            return JsonResponse(data)
            
# endregion

# region TIPO ESTADO DE DEPARTAMENTO

# Query que retorna las tipoestados
class TipoEstadoQueryset(object):

    def get_tipoestados_queryset(self, request):
        tipoestados = TipoEstado.objects.all()
        return tipoestados

# vista de los tipo de estado depa
class ListTipoEstadoView(View):
    def get(self, request):
        estados_list = TipoEstado.objects.all()

        context = {
            "estados_list": estados_list
        }
        return render(request, "AppIntranet/tipo_estado/mantenedor_estado.html", context)

# Crea un nuevo tipo de material
class CreateTipoEstado(View):

    # @method_decorator(login_required())
    def get(self, request):
        form = TipoEstadoForm()
        context = {
            'form': form,
            'success_message': ''
        }
        return render(request, 'AppIntranet/tipo_estado/add_tipoestado.html', context)

    # @method_decorator(login_required())
    def post(self, request):
        tipo = TipoEstado()
        form = TipoEstadoForm(request.POST, instance=tipo)
        nombre = request.POST['nombre']
        possible_tipos = TipoEstado.objects.filter(nombre=nombre)
        typ = possible_tipos[0] if len(possible_tipos) == 1 else None
        if typ is None:
            if form.is_valid():
                form.save()
                data = {
                    'mensaje': 'El Tipo de estado fue registrado correctamente.',
                    'type': 'success',
                    'tittle': 'Tipo estado de departamento'
                }
                return JsonResponse(data)
            
            else:
                data = {
                    'mensaje': 'El Tipo de Estado no se pudo registrar!.',
                    'type': 'error',
                    'tittle': 'Tipo estado de departamento'
                }
                return JsonResponse(data)
            
        else:
            data = {
                'mensaje': 'El Tipo de Estado ya existe!.',
                'type': 'error',
                'tittle': 'Tipo estado de departamento'
            }
            return JsonResponse(data)
            

# vista para editar el Tipo de material
class TipoEstadoEditView(View, TipoEstadoQueryset):
    def get(self, request, pk):
        possible_types = self.get_tipoestados_queryset(request).filter(pk=pk)
        typ = possible_types[0] if len(possible_types) == 1 else None
        if typ is not None:
            # cargamos el detalle
            context = {
                'form': TipoEstadoForm(instance=typ),
                'tipo': typ,
                'id': typ.pk,
            }
            return render(request, 'AppIntranet/tipo_estado/edit_tipoestado.html', context)
        else:
            data = {
                'mensaje': 'No existe el Tipo estado de departamento!.',
                'type': 'error',
                'tittle': 'Tipo estado de departamento'
            }
            return JsonResponse(data)

    def post(self, request, pk):
        possible_types = self.get_tipoestados_queryset(request).filter(pk=pk)
        typ = possible_types[0] if len(possible_types) == 1 else None
        if typ is not None:
            form = TipoEstadoForm(request.POST, instance=typ)
            if form.is_valid():
                tipo = TipoEstado.objects.filter(nombre=form.cleaned_data['nombre'])                    
                if len(tipo) == 0 :
                    form.save()
                    data = {
                        'mensaje': 'El tipo estado de departamento se editó correctamente!.',
                        'type': 'success',
                        'tittle': 'Tipo estado de departamento'
                    }
                    return JsonResponse(data)
                    
                else:
                    if tipo[0].pk == typ.pk:
                        form.save()
                        data = {
                            'mensaje': 'El tipo estado de departamento se editó correctamente!.',
                            'type': 'success',
                            'tittle': 'Tipo estado de departamento'
                        }
                        return JsonResponse(data)
                    else:
                        data = {
                            'mensaje': 'El Tipo de estado de departamento ya existe!.',
                            'type': 'error',
                            'tittle': 'Tipo estado de departamento'
                        }
                        return JsonResponse(data)
                
                   
                
            else:
                data = {
                    'mensaje': 'No se puedo editar!.',
                    'type': 'error',
                    'tittle': 'Tipo estado de departamento'
                }
                return JsonResponse(data)

# endregion

# region REGION

# Query que retorna las regiones
class RegionQueryset(object):

    def get_regiones_queryset(self, request):
        regiones = Region.objects.all()
        return regiones

# vista de las Regiones
class ListRegionView(View):
    def get(self, request):
        regiones_list = Region.objects.all()

        context = {
            "regiones_list": regiones_list
        }
        return render(request, "AppIntranet/list_regiones.html", context)

# endregion

# region COMUNA

# Query que retorna las comunas
class ComunaQueryset(object):

    def get_comunas_queryset(self, request):
        comunas = Comuna.objects.all()
        return comunas

# vista de las Regiones
class ListComunaView(View):
    def get(self, request):
        comunas_list = Comuna.objects.all()

        context = {
            "comunas_list": comunas_list
        }
        return render(request, "AppIntranet/list_comunas.html", context)

# endregion

# region IMAGENES DE DEPARTAMENTO

# Query que retorna las ImagenDepartamento
class ImagenDepartamentoQueryset(object):
    @classmethod
    def get_imagenes_queryset(cls):
        imagenes = ImagenDepartamento.objects.all()
        return imagenes

# vista de los ImagenDepartamento
class ListImagenDepartamentoView(View):
    def get(self, request,pk):
        imagenes_list = ImagenDepartamento.objects.filter(fk_departamento=pk)
        depa = Departamento.objects.filter(pk=pk)[0]

        context = {
            "imagenes_list": imagenes_list,
            'departamento':depa
        }
        request.session['pk_departamento'] = pk
        return render(request, "AppIntranet/imagen/mantenedor_imagen.html", context)

# Crea ImagenDepartamento
class CreateImagenDepartamentoView(View):

    #@method_decorator(login_required())
    def get(self, request):
        form = ImagenDepartamentoForm()
        pk = request.session.get('pk_departamento')
        depa = Departamento.objects.filter(pk=pk)[0]
        context = {
            'form': form,
            'departamento': depa,
            'pk': pk,
        }
        return render(request, 'AppIntranet/imagen/add_imagen.html', context)

    #@method_decorator(login_required())
    def post(self, request):

        pk = request.session.get('pk_departamento')
        if pk:
            possible_depas = Departamento.objects.filter(pk=pk)
            depa = possible_depas[0] if len(possible_depas) == 1 else None
            if depa is not None:
                            
                img = ImagenDepartamento()
                img.fk_departamento = depa
                form = ImagenDepartamentoForm(request.POST, request.FILES,  instance=img)
                
                if form.is_valid():
                    form.save()
                    data = {
                        'mensaje': 'La imagen fue guardada correctamente',
                        'type': 'success',
                        'tittle': 'Imagen Departamento'
                    }
                    return JsonResponse(data)

                else:
                    data = {
                        'mensaje': 'No se pudo agregar la imagen',
                        'type': 'error',
                        'tittle': 'Imagen Departamento'
                    }
                    return JsonResponse(data)
            else:
                data = {
                    'mensaje': 'Error al asignar pk depa',
                    'type': 'error',
                    'tittle': 'Imagen Departamento'
                }
                return JsonResponse(data)
        else:
            context = {
                'msg': 'No hay pk session'
            }
            return render(request, 'AppIntranet/imagen/add_imagen.html', context)

# Editar ImagenDepartamento
class EditImagenDepartamentoView(View):
    #@method_decorator(login_required())
    def get(self, request, pk):
        possible_imagenes = ImagenDepartamento.objects.filter(pk=pk)
        img = possible_imagenes[0] if len(possible_imagenes) == 1 else None
        form = ImagenDepartamentoForm()
        if img:
            form = ImagenDepartamentoForm(instance=img)
        pk_depa = request.session.get('pk_departamento')
        depa = Departamento.objects.filter(pk=pk_depa)[0]
        context = {
            'form': form,
            'pk': pk_depa,
            'id': img.pk,
            'departamento': depa,
        }
        return render(request, 'AppIntranet/imagen/edit_imagen.html', context)

    #@method_decorator(login_required())
    def post(self, request, pk):
        possible_imagenes = ImagenDepartamento.objects.filter(pk=pk)
        img = possible_imagenes[0] if len(possible_imagenes) == 1 else None
        
        form = ImagenDepartamentoForm(request.POST, request.FILES, instance=img)
        if form.is_valid():
            form.save()
            data = {
                'mensaje': 'La imagen fue editada correctamente',
                'type': 'success',
                'tittle': 'Imagen Departamento'
            }
            return JsonResponse(data)
        else:
            data = {
                'mensaje': 'No se pudo editar la imagen',
                'type': 'error',
                'tittle': 'Imagen Departamento'
            }
            return JsonResponse(data)
        

# endregion

# region MANTENCION

# Query que retorna las Mantencion
class MantencionQueryset(object):
    @classmethod
    def get_mantenciones_queryset(cls):
        mantenciones = Mantencion.objects.all()
        return mantenciones

# vista de los Mantencion
class ListMantencionView(View):
    def get(self, request,pk):
        mantenciones_list = Mantencion.objects.filter(fk_departamento=pk)
        cantidad = len(mantenciones_list)
        depa = Departamento.objects.filter(pk=pk)[0]
        context = {
            "mantenciones_list": mantenciones_list,
            'departamento': depa,
            'cantidad': cantidad,
        }
        request.session['pk_departamento'] = pk
        return render(request, "AppIntranet/mantencion/mantenedor_mantencion.html", context)

# Crea Mantencion
class CreateMantencionView(View):

    #@method_decorator(login_required())
    def get(self, request):
        form = MantencionForm()
        pk = request.session.get('pk_departamento')
        depa = Departamento.objects.filter(pk=pk)[0]
        context = {
            'form': form,
            'pk': pk,
            'departamento': depa,
        }
        return render(request, 'AppIntranet/mantencion/add_mantencion.html', context)

    #@method_decorator(login_required())
    def post(self, request):

        pk = request.session.get('pk_departamento')
        if pk:
            possible_depas = Departamento.objects.filter(pk=pk)
            depa = possible_depas[0] if len(possible_depas) == 1 else None
            users = Usuario.objects.filter(pk=request.user.pk)
            user = users[0] if len(users) == 1 else None
            #print(depa + " " + user)
            if (depa is not None) and (user is not None):
                            
                mant = Mantencion()
                mant.fk_departamento = depa
                mant.fk_empleado = user
                mant.fechaRegistro = date.today()
                mant.fechaRealInicio = date.today()
                mant.fechaRealTermino = date.today()
                form = MantencionForm(request.POST,  instance=mant)
                
                if form.is_valid():
                    form.save()
                    data = {
                        'mensaje': 'La mantencion fue registrada correctamente',
                        'type': 'success',
                        'tittle': 'Mantencion',
                        'pk': depa.pk,
                    }
                    return JsonResponse(data)

                else:
                    data = {
                        'mensaje': 'No se pudo agregar la mantencion',
                        'type': 'error',
                        'tittle': 'Mantencion'
                    }
                    return JsonResponse(data)
            else:
                data = {
                    'mensaje': 'Error al asignar pk depa/ user',
                    'type': 'error',
                    'tittle': 'Mantencion'
                }
                return JsonResponse(data)
        else:
            context = {
                'msg': 'No hay pk session'
            }
            return render(request, 'AppIntranet/mantencion/add_mantencion.html', context)

# Editar Mantencion
class EditMantencionView(View):
    #@method_decorator(login_required())
    def get(self, request, pk):
        possible_mantenciones = Mantencion.objects.filter(pk=pk)
        mant = possible_mantenciones[0] if len(possible_mantenciones) == 1 else None
        form = MantencionForm()
        if mant:
            form = MantencionForm(instance=mant)
        pk_depa = request.session.get('pk_departamento')
        depa = Departamento.objects.filter(pk=pk_depa)[0]
        context = {
            'form': form,
            'pk': pk_depa,
            'departamento': depa,
            'mantencion': mant,
        }
        return render(request, 'AppIntranet/mantencion/edit_mantencion.html', context)

    #@method_decorator(login_required())
    def post(self, request, pk):
        possible_mantenciones = Mantencion.objects.filter(pk=pk)
        mant = possible_mantenciones[0] if len(possible_mantenciones) == 1 else None
        
        form = MantencionForm(request.POST,request.FILES, instance=mant)
        if form.is_valid():
            form.save()
            data = {
                'mensaje': 'La mantencion fue editada correctamente',
                'type': 'success',
                'tittle': 'Mantencion'
            }
            return JsonResponse(data)
        else:
            data = {
                'mensaje': 'No se pudo editar la mantencion',
                'type': 'error',
                'tittle': 'Mantencion'
            }
            return JsonResponse(data)
        

# endregion
