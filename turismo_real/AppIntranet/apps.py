from django.apps import AppConfig


class AppintranetConfig(AppConfig):
    name = 'AppIntranet'
