from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

from django.conf.urls import url

from AppWeb.views import Create as CreateUser, LoginView, LogoutView, ActivacionView #Usuario
from AppWeb.views import ListDepartamentosView, DetailDepartamentoView, SearchDepaView #Departamento
from AppWeb.views import ReservaView, CreateReservaView, MisReservasView #pruebas
from AppWeb.views import ListOcupanteView, CreateOcupanteView, EditOcupanteView, DeleteOcupanteView #Ocupantes
from AppWeb.views import TransporteView #Transporte
from AppWeb.views import PagoExitosoView, PagoErrorView #Pago
from AppWeb.views import ServicioAddView


urlpatterns = [
    
    path('',views.index, name = "index"),
    
    #region Ocupante
    url(r'^ocupantes/(?P<pk>[0-9]+)$', ListOcupanteView.as_view(), name='ocupantes'), 
    url(r'^ocupantes/add$', CreateOcupanteView.as_view(), name='create_ocu'), 
    url(r'^ocupantes/edit/(?P<pk>[0-9]+)$', EditOcupanteView.as_view(), name='edit_ocu'), 
    url(r'^ocupantes/del/(?P<pk>[0-9]+)$', DeleteOcupanteView.as_view(), name='del_ocu'), 
    #endregion Ocupante

    #region reserva
    url(r'^reserva$', ReservaView.as_view(), name='reserva'), 
    url(r'^mis_reservas$', MisReservasView.as_view(), name='mis_reservas'), 
    url(r'^reserva/add/(?P<pk>[0-9]+)$', CreateReservaView.as_view(), name='reserva_depto'), 
    url(r'^reserva/servicio/(?P<pk>[0-9]+)$', ServicioAddView.as_view(), name='cont_serv'), 
    #end reserva

    #region Departamento
    url(r'^departamentos$', ListDepartamentosView.as_view(), name='deptos_list'), #Cargar departamentos
    url(r'^busca/departamento$', SearchDepaView.as_view(), name='busca_depto'), #Cargar departamentos
    url(r'^departamento/(?P<pk>[0-9]+)$', DetailDepartamentoView.as_view(), name='detail_depto'), #Detalle del departamento(ID)

    #endregion Departamento
    
    #region usuario
    url(r'^new_user$', CreateUser.as_view(), name='create_user'), #Registrarse
    url(r'^login$', LoginView.as_view(), name='users_login'), #Iniciar sesion
    url(r'^logout$', LogoutView.as_view(), name='users_logout'), #Cerrar sesion
    url(r'^activation/(?P<pk>[0-9]+)$', ActivacionView.as_view(), name='activation'), #Activar usuario
    #endregion usuario

    #Region transporte
    url(r'^Transporte$', TransporteView.as_view(), name='Transporte'), #Muestra el transporte
    #endregion transporte

    #region Pagp
    url(r'^pago/exitoso$', PagoExitosoView.as_view(), name='PagoExito'),
    url(r'^pago/error$', PagoErrorView.as_view(), name='PagoError'),
    #endregion
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
