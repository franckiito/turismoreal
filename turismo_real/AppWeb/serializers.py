from rest_framework import serializers
from AccesoDatos.models import Comuna

class ComunaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comuna
        fields = '__all__'