# -*- coding: utf-8 -*-
from django.http import HttpResponse, JsonResponse, response
from django.views.generic import View, ListView, CreateView
from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.db.models import Q
from django.contrib.auth import logout as django_logout, authenticate, login as django_login

from django.core.mail import EmailMessage
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string

from django.core.mail import EmailMultiAlternatives
from django.conf import settings

from AccesoDatos.models import Usuario, TipoUsuario
from AccesoDatos.forms import UsuarioForm, LoginForm, ReservaForm, OcupanteForm, DisponibilidadForm
from AccesoDatos.models import Departamento, Arriendo, DetalleArriendo, Reserva, ImagenDepartamento
from AccesoDatos.models import ServicioExtra, TipoEstado, Reserva, Ocupante, Mantencion

import datetime
from datetime import date


# Create your views here.
def index(request):
    servicios = ServicioExtra.objects.all()
    context = {
        'servicios': servicios
    }
    return render(request, 'AppWeb/index.html', context)

# vista de los Reserva
class ReservaView(View):

    def get(self, request):
        form = ReservaForm()
        context = {
            'form': form,
        }
        return render(request, 'AppWeb/reserva.html', context)

# vista de los Contratar servicio una vez creada la reserva
class ServicioAddView(View):

    def get(self, request, pk):
        return render(request, 'AppWeb/contratar_servicios.html')

# region OCUPANTE

# Query que retorna los Ocupante
class OcupanteQueryset(object):
    @classmethod
    def get_ocupantes_queryset(cls):
        ocupantes = Ocupante.objects.all()
        return ocupantes

# vista de los Ocupante


class ListOcupanteView(View):
    def get(self, request, pk):
        arriendos = Arriendo.objects.filter(pk=pk)
        arriendo = arriendos[0] if len(arriendos) == 1 else None

        detalles = DetalleArriendo.objects.filter(fk_arriendo=pk)
        detalle = detalles[0] if len(detalles) == 1 else None

        if (arriendo is not None) or (detalle is not None):
            ocupantes = Ocupante.objects.filter(fk_arriendo=pk)
            cantidad = len(ocupantes)

            request.session['pk_arriendo'] = pk

            context = {
                "ocupantes_list": ocupantes,
                "cantidad": cantidad,
                "detalle": detalle,
                "pk": pk
            }

            return render(request, "AppWeb/ocupante/ocupantes.html", context)

        else:
            cantidad = 0
            detalle = ""
            ocupantes = []
            context = {
                "ocupantes_list": ocupantes,
                "cantidad": cantidad,
                "detalle": detalle,
            }
            return render(request, "AppWeb/ocupante/ocupantes.html", context)

# Crea Ocupante


class CreateOcupanteView(View):

    @method_decorator(login_required())
    def get(self, request):
        pk = request.session.get('pk_arriendo')
        form = OcupanteForm()
        context = {
            'form': form,
            'pk': pk,
        }
        return render(request, 'AppWeb/ocupante/add_ocupante.html', context)

    @method_decorator(login_required())
    def post(self, request):
        pk = request.session.get('pk_arriendo')
        if pk:
            arriendos = Arriendo.objects.filter(pk=pk)
            arriendo = arriendos[0] if len(arriendos) == 1 else None
            if arriendo is not None:

                ocu = Ocupante()
                form = OcupanteForm(request.POST, instance=ocu)

                if form.is_valid():
                    print("formulario valido")
                    ocu = form.save(commit=False)
                    ocu.fk_arriendo = arriendo
                    ocu.save()

                    data = {
                        'mensaje': 'El ocupante fue ingresado correctamente',
                        'type': 'success',
                        'tittle': 'Ocupante'
                    }
                    return JsonResponse(data)

                else:
                    data = {
                        'mensaje': 'No se pudo agregar el ocupante',
                        'type': 'error',
                        'tittle': 'Ocupante'
                    }
                    return JsonResponse(data)
            else:
                data = {
                    'mensaje': 'No se encontro el arriendo',
                    'type': 'error',
                    'tittle': 'Ocupante'
                }
                return JsonResponse(data)
        else:
            context = {
                'msg': 'No hay pk session'
            }
            return render(request, 'AppWeb/ocupante/add_ocupante.html', context)

# Editar Ocupante HACER
class EditOcupanteView(View):

    @method_decorator(login_required())
    def get(self, request, pk):
        ocupantes = Ocupante.objects.filter(pk=pk)
        ocu = ocupantes[0] if len(ocupantes) == 1 else None
        ocupante = Ocupante.objects.filter(pk=pk)
        form = OcupanteForm()
        if ocu:
            form = OcupanteForm(instance=ocu)
        pk_arriendo = request.session.get('pk_arriendo')
        context = {
            'form': form,
            'ocupante': ocupante,
            'pk': pk_arriendo,
        }
        return render(request, 'AppWeb/ocupante/edit_ocupante.html', context)

    @method_decorator(login_required())
    def post(self, request, pk):
        ocupantes = Ocupante.objects.filter(pk=pk)
        ocu = ocupantes[0] if len(ocupantes) == 1 else None

        form = OcupanteForm(request.POST, instance=ocu)
        if form.is_valid():
            form.save()
            data = {
                'mensaje': 'El ocupante fue editado correctamente',
                'type': 'success',
                'tittle': 'Ocupante'
            }
            return JsonResponse(data)
        else:
            data = {
                'mensaje': 'No se pudo editar el ocupante',
                'type': 'error',
                'tittle': 'Ocupante'
            }
            return JsonResponse(data)

# Elimina Ocupante HACER
class DeleteOcupanteView(View):

    @method_decorator(login_required())
    def get(self, request, pk):
        ocupantes = Ocupante.objects.filter(pk=pk)
        ocu = ocupantes[0] if len(ocupantes) == 1 else None
        
        if ocu:
            try:
                ocu.delete()
                pk = request.session.get('pk_arriendo')

                return redirect('/ocupantes/' + pk)
            except:
                return redirect('/ocupantes/' + pk)

# endregion

# region Reserva

# Query que retorna los Arriendos
class ArriendoQueryset(object):
    @classmethod
    def get_arriendos_queryset(cls):
        arriendos = Arriendo.objects.all()
        return arriendos

# vista de los Reserva


class MisReservasView(View):

    @method_decorator(login_required())
    def get(self, request):

        arriendos = Arriendo.objects.filter(
            fk_usuario=request.user.pk).only('id').all()
        cantidad = len(arriendos)
        if cantidad > 0:
            arriendos = DetalleArriendo.objects.filter(
                fk_arriendo__in=arriendos)
            context = {
                'arriendos': arriendos,
                'cantidad': cantidad,
            }
            return render(request, 'AppWeb/misReservas.html', context)
        else:
            context = {
                'arriendos': arriendos,
                'cantidad': cantidad,
            }
            return render(request, 'AppWeb/misReservas.html', context)

# Crea reserva de un departamento
class CreateReservaView(View):

    #@method_decorator(login_required())
    def get(self, request, pk):

        user_form = UsuarioForm()
        form = ReservaForm()

        departamentos = Departamento.objects.filter(pk=pk)
        depa = departamentos[0] if len(departamentos) == 1 else None

        error = ""

        print(request.user)
        
        if depa is not None:
            
            context = {
                'user_form': user_form,
                'form': form,
                'depa': depa,
                'error': error
            }
            return render(request, 'AppWeb/reserva.html', context)
        else:
            error = "Error con departamento"
            context = {
                'error': error
            }
            return render(request, 'AppWeb/reserva.html', context)

    #@method_decorator(login_required())
    def post(self, request, pk):
                 
        form = ReservaForm(request.POST)


        usuarios = Usuario.objects.filter(pk=request.user.pk)
        user = usuarios[0]
        print(user)

        departamentos = Departamento.objects.filter(pk=pk)
        depa = departamentos[0] if len(departamentos) == 1 else None
        if depa is not None:

            #Add atributos to arriendo

            estados = TipoEstado.objects.filter(nombre="En Reserva")
            estado = estados[0] if len(estados) == 1 else None

            if estado is not None:
                try:
                    arriendo = Arriendo()

                    arriendo.fk_tipo_estado = estado

                    arriendo.totalArriendo = request.POST.get('totalArriendo','')
                    arriendo.totalMulta = 0
                    arriendo.totalServicio = 0
                    arriendo.fechaRegistro = date.today()
                    arriendo.cantDias = request.POST.get('cantDias')
                    arriendo.cantNinos = request.POST.get('cantNinos')
                    arriendo.cantAdultos = request.POST.get('cantAdultos')
                    arriendo.cantPersonas = request.POST.get('cantPersonas')

                    arriendo.fk_usuario = user
                    arriendo.save()

                    #print(arriendo.fk_usuario.rut + " = " + arriendo.fk_usuario.direccion)

                    #Add atributos to reserva
                    reserva = Reserva()
                    inicio = request.POST.get('fechaInicio')
                    inicio = datetime.datetime.strptime(inicio, "%m-%d-%Y").strftime("%Y-%m-%d")
                    reserva.fechaInicio = inicio

                    fin = request.POST.get('fechaFin')
                    fin = datetime.datetime.strptime(fin, "%m-%d-%Y").strftime("%Y-%m-%d")
                    reserva.fechaTermino = fin
                    reserva.costo = request.POST.get('abono') #30%

                    reserva.save()

                    #Add atributos to detalle arriendo
                    detalle_arriendo = DetalleArriendo()
                    detalle_arriendo.fk_arriendo = arriendo
                    detalle_arriendo.fk_reserva = reserva
                    detalle_arriendo.fk_departamento = depa               

                    detalle_arriendo.save()

                    data = {
                        'mensaje': 'Su reserva fue ingresada correctamente',
                        'type': 'success',
                        'tittle': 'Reserva'
                    }
                    return JsonResponse(data)
                except Exception as e:
                    data = {
                        'mensaje': e,
                        'type': 'error',
                        'tittle': 'Reserva'
                    }
                    return JsonResponse(data)

            else:
                data = {
                    'mensaje': 'No se agregar estado al arriendo',
                    'type': 'error',
                    'tittle': 'Tipo Estado reserva'
                }
                return JsonResponse(data)
        else:
            data = {
                'mensaje': 'Error con el departamento',
                'type': 'error',
                'tittle': 'Reserva'
            }
            return JsonResponse(data)
        

# endregion Reserva

# region Departamento

#Busca departamentos por fecha inicio y fin
class SearchDepaView(View):
    def post(self, request):

        print("Llega al metodo")
        fechaInicio = request.POST.get('fechaInicio','')
        print("Fecha inicio: " + fechaInicio)
        fechaInicio = datetime.datetime.strptime(fechaInicio, "%m-%d-%Y").strftime("%Y-%m-%d")
        fechaFin = request.POST.get('fechaFin','')
        fechaFin = datetime.datetime.strptime(fechaFin, "%m-%d-%Y").strftime("%Y-%m-%d")

        print("Inicio: " + fechaInicio + ", Fin: " + fechaFin)

        mantenciones = Mantencion.objects.all().exclude(fechaInicioProg__range=[fechaInicio,fechaFin]).only('fk_departamento_id')
        
        #Hay mantenciones hay que saber que id de depas son para no mandarlos a la lista de depa
        if(len(mantenciones) > 0):
            print(mantenciones)
        
        else:
            reservas = DetalleArriendo.objects.all(fk_reserva__fechaInicio=fechaInicio)
            print(reservas)
        
        
        data = {
            'mensaje': 'Buscando depa',
            'type': 'success',
            'tittle': 'Busqueda de disponibilidad'
        }
        return JsonResponse(data)
        

# Query que retorna los Departamento
class DepartamentoQueryset(object):
    @classmethod
    def get_departamentos_queryset(cls):
        departamentos = Departamento.objects.all()
        return departamentos

# vista de los Departamentos
class ListDepartamentosView(View):
    def get(self, request):
        departamentos_list = Departamento.objects.filter(estado=True)
        cantidad = len(departamentos_list)
        form = DisponibilidadForm() 
        context = {
            "departamentos_list": departamentos_list,
            "cantidad": cantidad,
            'form': form,
        }
        return render(request, "AppWeb/departamentos.html", context)

# vista detalle Departamento
class DetailDepartamentoView(View):
    def get(self, request, pk):

        possible_deptos = Departamento.objects.filter(pk=pk)
        departamento = possible_deptos[0] if len(
            possible_deptos) == 1 else None
        if departamento is not None:
            imagenes = ImagenDepartamento.objects.filter(fk_departamento=pk)
            servicios = ServicioExtra.objects.all()
            form = LoginForm()
            context = {
                'msj': '',
                'departamento': departamento,
                'imagenes': imagenes,
                'servicios': servicios,
                'form':form,
                'pk':pk
            }
            return render(request, 'AppWeb/detail_depto.html', context)
        else:
            context = {
                'msj': 'El departamento no existe',
                'departamento': departamento
            }
            # error 404
            return render(request, 'AppWeb/detail_depto.html', context)

# endregion Departamento

# region Usuario

# Login y logout


class LoginView(View):
    def get(self, request):
        error_messages = []
        form = LoginForm()
        context = {
            'errors': error_messages,
            'form': form,
        }
        return render(request, 'AppWeb/login.html', context)

    def post(self, request):
        
        error_messages = []
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('usr')
            password = form.cleaned_data.get('pwd', '')

            user = authenticate(username=username, password=password)
            
            if user is None:
                data = {
                    'mensaje': 'Nombre de usuario o contraseña incorrectos.',
                    'type': 'error',
                    'tittle': 'Inicio de sesion'
                }
                return JsonResponse(data)
            else:
                
                if user.is_active:
                    
                    django_login(request, user)
                    
                    #return render(request, "AppWeb/index.html")
                    data = {
                        'mensaje': 'Bienvenido a turismo real.',
                        'type': 'success',
                        'tittle': 'Inicio de sesion'
                    }
                    return JsonResponse(data)
                else:

                    data = {
                        'mensaje': 'Debe activar su cuenta.',
                        'type': 'error',
                        'tittle': 'Inicio de sesion'
                    }
                    return JsonResponse(data)
        else:
            data = {
                'mensaje': 'Registro de formulario invalido.',
                'type': 'error',
                'tittle': 'Inicio de sesion'
            }
            return JsonResponse(data)
        
# vista para cerrar sesion


class LogoutView(View):
    def get(self, request):
        if request.user.is_authenticated:
            django_logout(request)
        return redirect('index')

# vista para activar cuenta


class ActivacionView(View):
    # @method_decorator(login_required())
    def get(self, request, pk):
        possible_users = Usuario.objects.filter(
            pk=pk)  # .select_related('owner')
        usuario = possible_users[0] if len(possible_users) == 1 else None
        if usuario is not None:
            usuario.is_active = True
            usuario.save()
            django_login(request, usuario)
            # return render(request,'principal/index.html')
            data = {
                'mensaje': 'El usuario ha sido activado correctamente',
                'type': 'success',
                'tittle': 'Registro de Usuario'
            }
            return JsonResponse(data)
        else:
            data = {
                'mensaje': 'Erro al activador cuenta de usuario',
                'type': 'error',
                'tittle': 'Registro de Usuario'
            }
            return JsonResponse(data)

# Creacion de usuario(Registro, envio de correo con link para validar correo)


class Create(View):

    # @method_decorator(login_required())
    def get(self, request):
        """
        esto cmuestra un formulario para crear una foto
        :param request:
        :return:
        """
        form = UsuarioForm()
        context = {
            'form': form,
            'success_message': ''
        }
        return render(request, 'AppWeb/new_user.html', context)

    # @method_decorator(login_required())
    def post(self, request):
        """
        esto cmuestra un formulario para crear una foto y la crea
        :param request:
        :return:
        """

        username = request.POST['username']
        possible_users = Usuario.objects.filter(username=username)
        user = possible_users[0] if len(possible_users) == 1 else None
        if user is None:

            form = UsuarioForm(request.POST)
            if form.is_valid():

                user = form.save(commit=False)
                user.is_active = False
                clis = TipoUsuario.objects.filter(nombre="Cliente")
                cli = clis[0] if len(clis) == 1 else None
                if (cli is not None):

                    try:
                        user.fk_tipo_usuario = cli
                        user.save()
                        # print(user.pk)
                        html_content = "<a href=\"http://127.0.0.1:8000/activation/" + \
                            str(user.pk) + \
                            "\"> presiona aqui </a> para activar tu cuenta en Turismoreal.com"
                        # print(html_content)
                        message = EmailMultiAlternatives('Activacion de cuenta Turismo Real.',  # Titulo
                                                         'Confirme la cuenta registrada en Turismo Real',
                                                         settings.EMAIL_HOST_USER,  # Remitente
                                                         [user.email])  # Destinatario
                        message.attach_alternative(html_content, "text/html")
                        message.send()

                        data = {
                            'mensaje': 'Por favor confirmar correo electronico.',
                            'type': 'success',
                            'tittle': 'Registro de Usuario'
                        }
                        return JsonResponse(data)
                    except Exception as ex:
                        print(ex)
                        data = {
                            'mensaje': 'Error envio correo',
                            'type': 'error',
                            'tittle': 'Registro de Usuario'
                        }
                        return JsonResponse(data)

                else:
                    data = {
                        'mensaje': 'Error al cargar tipo usuario(cliente).',
                        'type': 'success',
                        'tittle': 'Registro de Usuario'
                    }
                    return JsonResponse(data)
            else:
                data = {
                    'mensaje': 'El formulario tiene problemas. ',
                    'type': 'error',
                    'username': username,
                    'tittle': 'Registro de Usuario'
                }
                return JsonResponse(data)
        else:
            data = {
                'mensaje': 'Usuario ya registrado.',
                'type': 'error',
                'tittle': 'Registro de Usuario'
            }
            return JsonResponse(data)

# Query que retorna los usuarios


class UsersQueryset(object):

    def get_users_queryset(self, request):

        users = Usuario.objects.all()
        return users

# vista para visualizar el detalle de un usuario


class UserDetailView(View, UsersQueryset):

    @method_decorator(login_required())
    def get(self, request, pk):
        """
        Carga la página de detalle de una foto
        :param request:
        :param pk:
        :return: HttpResponse
        """

        if request.user.is_staff:
            possible_users = self.get_users_queryset(
                request).filter(pk=pk)  # .select_related('owner')
            usuario = possible_users[0] if len(possible_users) == 1 else None
            if usuario is not None:
                # cargamos el detalle
                context = {
                    'usuario': usuario
                }
                return render(request, 'AppWeb/detail.html', context)
            else:
                # error 404
                return response.HttpResponseNotFound('No existe el usuario')
        else:
            redirect('index')

# endregion Usuario

# region Transporte

# vista de los tansportes
class TransporteView(View):

    def generate_request(url, params={}):

        response = requests.get(url, params=params)

        if response.status_code == 200:
            return response.json()

    def get_username(params={}):
        response = generate_request(
            'http://www.ejupi.co/api/v1/recorrido/', params)
        if response:
            user = response.get('results')[0]
            return user.get('name').get('first')

        return render(request, "AppWeb/transporte.html", context)

# vista Pago exitoso 123
class PagoExitosoView(View):

    def get(self, request):

        return render(request, 'AppWeb/pagoExito.html')

# vista Pago exitoso
class PagoErrorView(View):

    def get(self, request):

        return render(request, 'AppWeb/pagoError.html')

#endregion



#end region
